function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(0, 100, 250);
  noStroke();
    fill(255,255,0);
    ellipse(60,60,80);
  
  fill(255,0,0);
  rect(125, 250, 140, 140);
  
  fill(0,255,200);
  rect(240, 167, 25, 70); 

  fill(0,255,200);
  triangle(100, 250 , 290 , 250 , 197, 140);
  
  fill(100,0,0);
  rect(175, 310, 40, 80);

  fill(0,255,0);
  rect(0, 380, 400, 100);
  
}

